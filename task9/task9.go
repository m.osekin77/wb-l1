package main

import (
	"fmt"
)

// Simple pipeline implementation
// Because of unbuffered channels order is maintained,
// and each goroutine blocks until it receives the corresponding value

func Read(a *[5]int) <-chan int {
	in := make(chan int)
	go func() {
		defer close(in)
		for _, x := range *a {
			in <- x
		}
	}()
	return in
}

func Double(in <-chan int) <-chan int {
	out := make(chan int)
	go func() {
		defer close(out)
		for x := range in {
			out <- x * 2
		}
	}()
	return out
}

func PrintResult(out <-chan int) {
	for x := range out {
		fmt.Println(x)
	}
}

func runPipeline() {
	a := [5]int{1, 2, 3, 4, 5}
	in := Read(&a)
	out := Double(in)
	PrintResult(out)
}

func main() {
	runPipeline()
}
