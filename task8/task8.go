package main

import (
	"fmt"
	"strconv"
)

func main() {
	var x, bitpos, val int64
	fmt.Print("enter digit: ")
	fmt.Scan(&x)

	fmt.Print("enter bit position: ")
	fmt.Scan(&bitpos)

	fmt.Print("0 or 1 ? ")
	fmt.Scan(&val)

	fmt.Printf("x before bin %v, dec %v\n", strconv.FormatInt(x, 2), x)
	x = setBit(x, bitpos, val)
	fmt.Printf("x after bin %v, dec %v", strconv.FormatInt(x, 2), x)
}

func setBit(x, bitpos, val int64) int64 {
	// create a bitmask where the ith bit is set to 1
	// bitpos = 4, => 1 << 4 = 10000
	var mask, res int64 = 1 << bitpos, 0
	if val == 1 {
		// to set the ith bit, use `or` operator with the mask
		//   1001
		// | 0010
		//   1011
		res = x | mask
	} else {
		//  find inverse mask
		// ^(0010) = 1101

		//  then use `and` operator
		//   1011 - x
		// & 1101 - newmask
		//   1001 - res

		res = x &^ mask
	}
	return res
}
