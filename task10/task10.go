package main

import (
	"fmt"
)

func group(temps []float64, step int) map[int][]float64 {
	res := make(map[int][]float64)
	for _, t := range temps {
		key := (int(t) / step) * step
		res[key] = append(res[key], t)
	}
	return res
}

func main() {
	temps := []float64{-25.4, -27.0, 13.0, 19.0, 15.5, 24.5, -21.0, 32.5}
	res := group(temps, 10)
	fmt.Println(res)
}
