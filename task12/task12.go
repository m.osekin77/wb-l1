package main

import "fmt"

func unique(seq []string) map[string]struct{} {
	// only add unique elements
	set := make(map[string]struct{})
	for _, s := range seq {
		set[s] = struct{}{}
	}
	return set
}

func main() {
	seq := []string{"cat", "cat", "dog", "cat", "tree"}
	fmt.Println(unique(seq))
}
