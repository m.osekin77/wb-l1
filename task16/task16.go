package main

import "fmt"

// place elements which are less than pivot to the left, greater to the right
func partition(a []int, low, high int) ([]int, int) {
	pivot := a[high]
	i := low
	for idx := i; idx < high; idx++ {
		if a[idx] < pivot {
			a[i], a[idx] = a[idx], a[i]
			i++
		}
	}
	//
	a[i], a[high] = a[high], a[i]
	return a, i
}

// proceed recursively for left and right part
func qsort(a []int, low, high int) []int {
	if low < high {
		var p int
		a, p = partition(a, low, high)
		a = qsort(a, low, p-1)
		a = qsort(a, p+1, high)
	}
	return a
}

// wrapper function
func Qsort(a []int) []int {
	return qsort(a, 0, len(a)-1)
}

func main() {
	a := []int{8, 10, 4, 9, 5, 7, -1, -2, 12345, 54321}
	fmt.Println(Qsort(a))
}
