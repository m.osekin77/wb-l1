package main

import (
	"fmt"
)

func reverse(str string) string {
	// since string is represented as sequence of byte characters, we need to convert it utf-8 representation.
	res := []rune(str)
	n := len(res)
	i, j := 0, n-1
	for i < j {
		res[i], res[j] = res[j], res[i]
		i++
		j--
	}
	return string(res)
}

func main() {
	s := "главрыба"
	fmt.Println(reverse(s))
}
