package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

// Mutex approach is fine, but on the scale in highly concurrent environment
// there might be a big overhead on the system because of permanent mutex locks.
// In those cases atomics might be a preferable option.

type MCounter struct {
	mu  sync.RWMutex
	cnt int64
}

type ACounter struct {
	cnt int64
}

func NewMCounter() *MCounter {
	return &MCounter{
		mu:  sync.RWMutex{},
		cnt: 0,
	}
}

func NewACounter() *ACounter {
	return &ACounter{cnt: 0}
}

func (mc *MCounter) Add(tasks int64) {
	mc.mu.Lock()
	defer mc.mu.Unlock()
	mc.cnt += tasks
}

func (ac *ACounter) Add(tasks int64) {
	atomic.AddInt64(&ac.cnt, tasks)
}

func main() {
	mc := NewMCounter()
	numWorkers := 100
	var tasks int64 = 15
	wg := &sync.WaitGroup{}
	for i := 0; i < numWorkers; i++ {
		wg.Add(1)
		go workerM(wg, mc, tasks)
	}

	ac := NewACounter()
	for i := 0; i < numWorkers; i++ {
		wg.Add(1)
		go workerA(wg, ac, tasks)
	}

	wg.Wait()
	fmt.Println(mc.cnt, ac.cnt)
}

func workerM(wg *sync.WaitGroup, mc *MCounter, tasks int64) {
	defer wg.Done()
	mc.Add(tasks)
}

func workerA(wg *sync.WaitGroup, ac *ACounter, tasks int64) {
	defer wg.Done()
	ac.Add(tasks)
}
