package main

import (
	"fmt"
	"strings"
)

func createHugeString(size int) string {
	var str strings.Builder
	for i := 0; i < size; i++ {
		str.WriteRune('a')
	}
	return str.String()
}

func main() {
	v := createHugeString(1 << 10)
	// justString := v[:100]
	// CONS:
	// 1. Because justString references the original huge string, original string won't be
	// garbage collectd until justString exists, which might lead to memory leak.
	// 2. Any change on the original huge string will be reflected upon the justString
	// which might lead to the unexpected behavior.
	// SOLUTION:
	// create a copy of just string, so it does not reference the original string.
	justString := make([]byte, 100)
	copy(justString, v[:100])
	fmt.Println(string(justString))
}
