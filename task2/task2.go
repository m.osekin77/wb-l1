package main

import (
	"fmt"
	"sync"
)

func computeSquares(x int, wg *sync.WaitGroup) {
	// decrease the counter and print the number
	defer wg.Done()
	fmt.Println(x * x)
}

func main() {
	a := [...]int{2, 4, 6, 8, 10}
	wg := &sync.WaitGroup{}

	for _, x := range a {
		wg.Add(1)
		go computeSquares(x, wg)
	}
	// wait for all goroutines to finish
	wg.Wait()
}
