package main

import (
	"context"
	"log"
	"sync"
	"time"
)

func stopWithContext() {
	ctx, cancel := context.WithCancel(context.Background())
	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			select {
			case <-ctx.Done():
				log.Println("stopWithContext returned")
				return
			default:
				log.Println("stopWithContext working for 3 seconds")
				time.Sleep(3 * time.Second)
				cancel()
			}
		}
	}()
	wg.Wait()
}

func stopWithChannel() {
	exit := make(chan struct{}, 1)
	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			select {
			case <-exit:
				log.Println("stopWithChannel returned")
				return
			default:
				log.Println("stopWithChannel working for 3 seconds")
				time.Sleep(3 * time.Second)
				close(exit) // exit <-struct{}{}
			}
		}
	}()
	wg.Wait()
}

type DataG struct {
	flag bool
	mu   sync.RWMutex
}

func NewDataG() *DataG {
	return &DataG{
		flag: false,
		mu:   sync.RWMutex{},
	}
}

func (d *DataG) setFlag() {
	d.mu.Lock()
	defer d.mu.Unlock()
	d.flag = true
}

func (d *DataG) getFlag() bool {
	d.mu.RLock()
	defer d.mu.RUnlock()
	return d.flag
}

func stopWithFlag() {
	data := NewDataG()
	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			if data.flag {
				log.Println("stopWithFlag returned")
				return
			}
			log.Println("stopWithFlag working for 3 seconds")
			time.Sleep(3 * time.Second)
			data.setFlag()
		}
	}()
	wg.Wait()
}

func main() {
	stopWithContext()
	stopWithChannel()
	stopWithFlag()
}
