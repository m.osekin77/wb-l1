package main

import (
	"fmt"
	"log"
	"math/big"
)

func main() {
	a, err := new(big.Int).SetString("37218631298736478923643278931278", 10)
	if !err {
		log.Printf("%#v", err)
	}
	b, err := new(big.Int).SetString("11231231850945830594385034958340", 10)
	if !err {
		log.Printf("%#v", err)
	}

	sum := new(big.Int).Add(a, b)
	diff := new(big.Int).Sub(a, b)
	product := new(big.Int).Mul(a, b)
	div := new(big.Int).Div(a, b)

	fmt.Println("sum: ", sum)
	fmt.Println("diff: ", diff)
	fmt.Println("product: ", product)
	fmt.Println("div: ", div)
}
