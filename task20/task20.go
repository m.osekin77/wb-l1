package main

import (
	"fmt"
	"strings"
)

func reverseStringsB(s string) string {
	strs := strings.Split(s, " ")
	var res strings.Builder
	for i := len(strs) - 1; i >= 0; i-- {
		if i < len(strs)-1 {
			res.WriteString(" ")
		}
		res.WriteString(strs[i])
	}
	return res.String()
}

func reverseStringsP(s string) string {
	strs := strings.Split(s, " ")
	i, j := 0, len(strs)-1
	for i < j {
		strs[i], strs[j] = strs[j], strs[i]
		i++
		j--
	}
	return strings.Join(strs, " ")
}

func main() {
	s := "snow dog sun"
	fmt.Println(reverseStringsB(s), reverseStringsP(s))
	fmt.Println(reverseStringsB(s) == reverseStringsP(s)) // true
}
