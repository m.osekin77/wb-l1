package main

import (
	"fmt"
	"math"
)

type Point struct {
	x, y float64
}

func NewPoint(x, y float64) Point {
	return Point{
		x: x,
		y: y,
	}
}

func EuclidianDistance(p1 Point, p2 Point) float64 {
	return math.Sqrt(math.Pow(p1.x-p2.x, 2) + math.Pow(p1.y-p2.y, 2))
}

func main() {
	p1, p2 := NewPoint(10.35, 18.76), NewPoint(5.64, -3.25)
	fmt.Println(EuclidianDistance(p1, p2))
}
