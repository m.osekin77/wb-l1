package main

import "fmt"

func intersect(a1, a2 []int) []int {
	// use struct{} because it is more memory efficient
	set := make(map[int]struct{})
	for _, x := range a1 {
		set[x] = struct{}{}
	}
	var res []int
	for _, x := range a2 {
		if _, ok := set[x]; ok {
			res = append(res, x)
		}
	}
	return res
}

func main() {
	a1, a2 := []int{1, 2, 3, 4, 5}, []int{1, 3, 5, 100}
	fmt.Println(intersect(a1, a2))
}
