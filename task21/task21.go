package main

import "fmt"

type Computer interface {
	InsertLightning()
}

type Mac struct{}

func (m *Mac) InsertLightning() {
	fmt.Println("lightning cable plugged in")
}

type PC struct{}

func (w *PC) InsertUSB() {
	fmt.Println("usb-c cable plugged in")
}

type PCAdapter struct {
	pc *PC
}

func (pca *PCAdapter) InsertLightning() {
	// This is the most important part, we can imagine it translates the signal
	// ,and we will be able to use the usb-c cable.
	pca.pc.InsertUSB()
}

type Client struct{}

// We have only lightning cable, MAC and PC. PC only support usb-c, but we want
// to use lightning cable on it, so we create an adapter, we can think of it as a hub.

func (c *Client) InsertPort(pc Computer) {
	pc.InsertLightning()
}

func main() {
	client := &Client{}
	mac := &Mac{}
	client.InsertPort(mac)
	pc := &PC{}
	pcAdapter := &PCAdapter{
		pc: pc,
	}
	// insert port into hub
	client.InsertPort(pcAdapter)
}
