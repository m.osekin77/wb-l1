package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

/*
When doing concurrent and parallel operations, it's always important to remember about memory safety
so we won't encounter any data race conditions. In golang, sync package allows us to do that.
*/

func SumSquaresAtomic(x int, sum *int64, wg *sync.WaitGroup) {
	defer wg.Done()
	atomic.AddInt64(sum, int64(x*x))
}

func SumSquaresMutex(x int, sum *int64, mu *sync.Mutex, wg *sync.WaitGroup) {
	defer wg.Done()
	mu.Lock()
	*sum += int64(x * x)
	mu.Unlock()
}

func main() {
	a := [...]int{2, 4, 6, 8, 10}
	wg := &sync.WaitGroup{}

	var sumAtomic int64
	for _, x := range a {
		wg.Add(1)
		go SumSquaresAtomic(x, &sumAtomic, wg)
	}

	mu := &sync.Mutex{}
	var sumMutex int64
	for _, x := range a {
		wg.Add(1)
		go SumSquaresMutex(x, &sumMutex, mu, wg)
	}
	wg.Wait()

	fmt.Println(sumMutex, sumAtomic, sumMutex == sumAtomic) // 220, 220, true

}
