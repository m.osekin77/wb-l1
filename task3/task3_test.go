package main

import (
	"sync"
	"testing"
)

// Atomics require less bytes per operation, but other statistics is pretty much the same

//goos: darwin
//goarch: arm64
//pkg: wb-l1/task3
//Benchmark/atomic-8         	 821511	     1274 ns/op	    160 B/op	      5 allocs/op
//Benchmark/mutex-8          	 901447	     1317 ns/op	    240 B/op	      5 allocs/op
//PASS
//ok  	wb-l1/task3	2.268s

func Benchmark(b *testing.B) {
	var sumAtomic, sumMutex int64
	a := [...]int{2, 4, 6, 8, 10}
	wg := &sync.WaitGroup{}
	mu := &sync.Mutex{}

	b.Run("atomic", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			for _, x := range a {
				wg.Add(1)
				go SumSquaresAtomic(x, &sumAtomic, wg)
			}
			wg.Wait()
		}
	})

	b.Run("mutex", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			for _, x := range a {
				wg.Add(1)
				go SumSquaresMutex(x, &sumMutex, mu, wg)
			}
			wg.Wait()
		}
	})

}
