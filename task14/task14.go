package main

import (
	"fmt"
	"reflect"
)

type T struct{}

func NewT() *T {
	return &T{}
}

func getType(x any) string {
	return reflect.TypeOf(x).String()
}

func main() {
	ch := make(chan any)
	mp := make(map[any]any)
	t := NewT()
	a := []any{5, "a", true, struct{}{}, ch, mp, t}
	// use reflect to get type info
	for _, x := range a {
		fmt.Println(getType(x))
	}
	// use type switch
	for _, x := range a {
		switch v := x.(type) {
		case int:
			fmt.Println("int", v)
		case string:
			fmt.Println("string", v)
		case bool:
			fmt.Println("bool", v)
		case struct{}:
			fmt.Println("struct{}", v)
		case chan any:
			fmt.Println("chan any", v)
		case map[any]any:
			fmt.Println("map[any]any", v)
		case *T:
			fmt.Println("T pointer", v)
		default:
			fmt.Println("unknown type")
		}
	}
}
