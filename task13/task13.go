package main

import "fmt"

// will fail for big numbers
func swapD(a, b *int) {
	*a = abs(*a - *b)
	*b -= *a
	*a += *b
}

// a - 1100, b - 1111,
// 1100 ^ 1111 = 0011 -> mask
// 0011 ^ 1111 = 1100 -> b is now a
// 1100 ^ 1100 = 1111 -> a is b now
func swapXor(a, b *int) {
	*a = *a ^ *b
	*b = *a ^ *b
	*a = *a ^ *b
}

func main() {
	var a, b int
	fmt.Println("enter numbers")
	fmt.Scan(&a, &b)
	fmt.Printf("a=%v b=%v\n", a, b)
	swapXor(&a, &b)
	fmt.Printf("a=%v b=%v\n", a, b)
}

func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}
