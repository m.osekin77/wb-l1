package main

import "fmt"

type Human struct {
	name       string
	age        int
	numPushUps int
	numKM      int
}

/*
   There is no direct inheritance in GO, but we can achieve
   the similar effect with struct embedding.
*/

type Action struct {
	Human
}

func (a *Action) doPush(x int) {
    /*
	We have direct access to the Human struct fields,
	no need to explicitly write selectors
    */

	a.numPushUps += x // a.Human.numPushUps += x
}

func (h *Human) runKM(x int) {
	// By defining methods on Human, we also define them for Action type.
	h.numKM += x
}

func main() {
	h := Human{
		name: "artem",
		age:  21,
		// numPushUps: 0, uninitialized fields have default values
		// numKM: 0,
	}

	a := Action{
		h,
	}

	a.runKM(10)   // modifies human struct fields a.Human.runKM(10)
	a.doPush(100) // also modifies human struct fields

	fmt.Println(a.Human) // {artem 21 100 10}
}
