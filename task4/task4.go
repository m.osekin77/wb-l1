package main

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"sync"
	"time"
)

/*
In this example I choose to cancel all workers with the usage of context package.
I create a child context with additional properties from the parent context and pass it to all workers, so when interruption signal is received, each of the workers knows that
it should return.
I use sync.WaitGroup, so that after cancellation signal is received, main goroutine waits for all of the workers to stop,
I think it is important, because every goroutine has to release its resources it is holding from the OS.
*/

func main() {
	var numWorkers int
	fmt.Println("Enter number of workers: ")
	fmt.Scan(&numWorkers)

	mainChannel := make(chan int)
	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, os.Interrupt)
	ctx, cancel := context.WithCancel(context.Background())

	wg := &sync.WaitGroup{}
	for id := 0; id < numWorkers; id++ {
		wg.Add(1)
		go worker(mainChannel, ctx, wg, id)
	}

	wg.Add(1)
	go genData(mainChannel, wg, ctx)
	<-shutdown
	cancel()
	wg.Wait()
	fmt.Println("all workers finished")
}

func genData(ch chan<- int, wg *sync.WaitGroup, ctx context.Context) {
	defer wg.Done()
	defer close(ch)
	for {
		select {
		case <-ctx.Done():
			fmt.Println("data generation stopped")
			return
		default:
			ch <- rand.Intn(100) + 1
		}
	}
}

func worker(ch <-chan int, ctx context.Context, wg *sync.WaitGroup, id int) {
	defer wg.Done()
	for {
		select {
		case x, ok := <-ch:
			if ok {
				fmt.Printf("worker with id %v recieved %v\n", id, x)
				time.Sleep(3 * time.Second)
			}
		case <-ctx.Done():
			fmt.Printf("worky with id %v stopped\n", id)
			return
		}
	}
}
