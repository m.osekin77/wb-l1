package main

import "fmt"

func main() {
	a := []int{1, 2, 3, 4, 5}
	var idx int
	fmt.Scan(&idx)
	if idx >= len(a) {
		fmt.Println("invalid range")
		return
	}
	fmt.Println("before ", a)
	a = append(a[:idx], a[idx+1:]...)
	fmt.Println("after ", a)
}
