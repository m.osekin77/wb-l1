package main

import (
	"context"
	"fmt"
	"log"
	"sync"
	"time"
)

func main() {
	var t int
	log.Println("enter number of seconds: ")
	fmt.Scan(&t)

	ch := make(chan int)
	// we create timeout channel which will be received by the main goroutine
	// which will trigger termination of the program.
	timeout := time.After(time.Duration(t) * time.Second)
	ctx, cancel := context.WithCancel(context.Background())
	wg := &sync.WaitGroup{}
	log.Printf("programm will stop after %v\n seconds", t)

	wg.Add(2)
	go sender(ctx, wg, ch)
	go receiver(ctx, wg, ch)

	<-timeout
	cancel()
	wg.Wait()
	log.Println("all goroutines exited")
}

func sender(ctx context.Context, wg *sync.WaitGroup, ch chan<- int) {
	defer wg.Done()
	for i := 100; ; i-- {
		select {
		case <-ctx.Done():
			log.Println("sender stopped")
			return
		default:
			ch <- i
			time.Sleep(1 * time.Second)
		}
	}
}

func receiver(ctx context.Context, wg *sync.WaitGroup, ch <-chan int) {
	defer wg.Done()
	for {
		select {
		case x, ok := <-ch:
			if ok {
				log.Printf("received %v\n", x)
			}
		case <-ctx.Done():
			log.Println("receiver stopped")
			return
		}
	}
}
