package main

import (
	"crypto/rand"
	"fmt"
	"sync"
)

// Cache uses RWMutex for safe concurrent read and write to the map.
type Cache struct {
	data map[int]string
	mu   sync.RWMutex
}

func NewCache() *Cache {
	return &Cache{
		data: make(map[int]string),
		mu:   sync.RWMutex{},
	}
}

func (c *Cache) Write(id int, data string) {
	c.mu.Lock() // Block Write for other goroutines until successful write operation.
	defer c.mu.Unlock()
	c.data[id] = data
}

func (c *Cache) Get(id int) string {
	c.mu.RLock() // Block Read for other goroutines until successful read operation.
	defer c.mu.RUnlock()
	return c.data[id]
}

func genString(length int) string {
	charset := "abcdefghijklmnopqrstuvwxyz"
	bytes := make([]byte, length)
	_, _ = rand.Read(bytes)
	for i := range bytes {
		bytes[i] = charset[int(bytes[i])%len(charset)]
	}
	return string(bytes)
}

const (
	n  = 10
	sz = 5
)

func main() {
	cache := NewCache()
	wg := &sync.WaitGroup{}
	data := make(chan int)

	// generate 10 random strings
	wg.Add(1)
	go func() {
		defer wg.Done()
		defer close(data)
		for i := 0; i < n; i++ {
			cache.Write(i, genString(sz))
			data <- i
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		for x := range data {
			fmt.Println(cache.Get(x))
		}
	}()
	wg.Wait()
}
