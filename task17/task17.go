package main

import "fmt"

func binarySearch(a []int, target int) (int, bool) {
	l, r := 0, len(a)
	for r-l > 1 {
		m := (l + r) >> 1
		if a[m] <= target {
			l = m
		} else {
			r = m
		}
	}
	if a[l] == target {
		return l, true
	}
	return -1, false
}

func main() {
	a := []int{3, 5, 7, 9, 15, 17}
	fmt.Println(binarySearch(a, 7))  // 2, true
	fmt.Println(binarySearch(a, 13)) // -1, false
	fmt.Println(binarySearch(a, 17)) // 5, true
}
