package main

import (
	"errors"
	"fmt"
	"syscall"
	"time"
)

func sleep(sec time.Duration) {
	<-time.After(sec)
}

func mySleep(seconds int64) error {
	timespec := &syscall.Timespec{
		Sec: seconds,
	}
	var remaining syscall.Timespec
	for {
		err := syscall.Nanosleep(timespec, &remaining)
		if err == nil {
			break
		}
		if !errors.Is(err, syscall.EINTR) {
			return err
		}
		*timespec = remaining
	}
	return nil
}

func main() {
	fmt.Println("sleeping for 5 seconds")
	sleep(5 * time.Second)
	mySleep(5)
	fmt.Println("good morning")
}
