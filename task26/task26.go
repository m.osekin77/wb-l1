package main

import (
	"fmt"
	"strings"
)

func unique(word string) bool {
	has := make(map[rune]struct{})
	for _, v := range strings.ToLower(word) {
		if _, ok := has[v]; ok {
			return false
		}
		has[v] = struct{}{}
	}
	return true
}

func main() {
	a := []string{"abcd", "abCdefAaf", "aabcd"}
	for _, str := range a {
		fmt.Println(unique(str))
	}
}
